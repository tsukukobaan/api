#!/usr/bin/env python
# -*- coding: utf-8 -*-

#importing modules for function
import sys
import urllib2
from xml.etree.ElementTree import *
import xml.etree.ElementTree as ET

#function
def api(a):
	url = 'http://zip.cgis.biz/xml/zip.php?zn='
	address = str(a)
	response = urllib2.urlopen(url+address)
	return response

def make_dict(num):
	response=api(num)
	data = response.read()
	root = ET.fromstring(data)
	result = {}
	for value in root.iter('value'):
		for k,v in value.attrib.items():
			#print k , v
			nome = k
			basho = v
			result.update({nome:basho})
	#state = data['state']
	#city = data['city']
	#address = data['address']
	return result
	


#importing modules for Django
from django.http import HttpResponse
from django.template import RequestContext,loader,Context,Template

from django.shortcuts import render
from django.http import HttpResponseRedirect
from finda.models import Post

#Django views
def index(request):
	template = loader.get_template('finda/index.html')
	context = {}
	return render(request,'finda/index.html',context)
	
def results(request):
	if request.method == 'POST':
		form = Post(request.POST)
		#form = str(form)
		#form = request.POST.get('number')
		num = request.POST.get('number')
		data = make_dict(num)
	else:
		form = Post()
	return render(request,'finda/results.html',{
		'form':form,
		'num':num,
		'data':data,
	})