from django.conf.urls import patterns,url
from finda import views

urlpatterns = patterns('',
	#ex: /findaddress/
	url(r'^$',views.index,name='index'),
	#ex: /findaddress/results/
	url(r'^results/$',views.results,name='results'),
	)