from django.db import models
from django import forms
from django.forms import ModelForm

'''
class Post(forms.Form):
	number = forms.CharField()
'''

class Post(models.Model):
    number = models.CharField(max_length=10)
    def __unicode__(self):
    	return self.number

class Results(models.Model):
	all = models.CharField(max_length=50)
	state = models.CharField(max_length=10)
	city = models.CharField(max_length=30)
	address = models.CharField(max_length=30)
	def __unicode__(self):
		return self.all

class PostForm(ModelForm):
	class Meta:
		model = Post